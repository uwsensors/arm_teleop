#include <stdio.h>
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>

static const std::string COMMAND_TOPIC = "r_cart/command_pose";

int main(int argc, char** argv) {

	ros::init(argc, argv, "arm_pub_test");
	ros::NodeHandle node;

	// Publish poses that r_gripper_tool_frame should be moved to. The controller subscribes to this topic
	ros::Publisher pub = node.advertise<geometry_msgs::PoseStamped>(COMMAND_TOPIC,1);
	geometry_msgs::PoseStamped pose;
	pose.header.frame_id = "r_gripper_tool_frame";
	pose.header.stamp = ros::Time(0);
	pose.pose.position.x = 0.0;
	pose.pose.position.y = 0.0;
	pose.pose.position.z = 0.0;
	pose.pose.orientation.x = 0.0;
	pose.pose.orientation.y = 0.0;
	pose.pose.orientation.z = 0.0;
	pose.pose.orientation.w = 1.0;

	while(ros::ok()) {		
		std::string cmd;
		std::cin >> cmd; // Wait for string from command line
		std::string cmdBegin(1,cmd[0]);
		if(cmdBegin.compare("w")==0) {
			pose.pose.position.x = 0.05; // Specify move forwards by 5cm
			pub.publish(pose);	// Publish the pose
		} else if(cmdBegin.compare("s")==0) {
			pose.pose.position.x = -0.05; // Specify move backwards by 5cm
			pub.publish(pose);	// Publish the pose
		}	
    	ros::Duration(0.1).sleep();
	}
	return 0;

}
